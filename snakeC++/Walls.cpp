#include "stdafx.h"
#include "Walls.h"
#include "HorizontalLine.h"
#include "VerticalLine.h"


Walls::Walls(void)
{
}


Walls::~Walls(void)
{
}


Walls::Walls( COORD screenSize )
{

	//��������� ��� �������� ����
	HorizontalLine upLine( 0, screenSize.X - 2, 0, '-' );
	HorizontalLine downLine( 0, screenSize.X - 2, screenSize.Y - 1, '-' );
	VerticalLine leftLine( 0, 1, screenSize.Y - 2, '|' );
	VerticalLine rightLine( screenSize.X - 1, 1, screenSize.Y - 2, '|' );

		/*HLine topBorder(0,78,0,'-');
	topBorder.Draw();
	HLine bottomBorder(0,78,24,'-');
	bottomBorder.Draw();
	VLine leftBorder(0,1,23,'|');
	leftBorder.Draw();
	VLine rightBorder(79,1,23,'|');
	rightBorder.Draw();*/

	wallList.push_back( upLine );
	wallList.push_back( downLine );
	wallList.push_back( leftLine );
	wallList.push_back( rightLine );
}

bool Walls::IsHit( Figure figure)
{
	std::list<Figure>::iterator wall;
	for(wall = wallList.begin(); wall != wallList.end(); wall++)
	{
		if(wall->IsHit(figure))
		{
			return true;
		}
	}
	return false;
}

void Walls::Draw()
{
	std::list<Figure>::iterator wall;
	for(wall = wallList.begin(); wall != wallList.end(); wall++)
	{
		wall->Draw();
	}
}