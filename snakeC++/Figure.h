#pragma once
#include "Point.h"

class Figure
{
protected:
	std::list<Point> pList;
	bool IsHit(Point);
public:
	Figure(void);
	~Figure(void);
	void Draw(void);
	bool IsHit(Figure);
};

