#pragma once
class Snake : public Figure
{
	Direction direction;
public:
	Snake(Point tail, int length, Direction _direction);
	~Snake(void);
	void Move();
	inline Point Snake::GetNextPoint();
	void HandleKey(int key);
	bool Eat(Point food);
	bool IsHitTail();
};

