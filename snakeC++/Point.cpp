#include "stdafx.h"
#include "Point.h"

HANDLE Point::hOuput = GetStdHandle(STD_OUTPUT_HANDLE);

Point::Point(int _x, int _y, char _sym)
{
	this->pos.X=_x;
	this->pos.Y=_y;
	this->sym=_sym;
}

Point::~Point(void)
{
}


void Point::Draw ()
{
	SetConsoleCursorPosition(this->hOuput,this->pos);
	std::cout<<this->sym;
}


void Point::Move(int offset, Direction direction){

	if(direction == RIGHT)
	{
		pos.X = pos.X + offset;
	}
	else if(direction == LEFT)
	{
		pos.X = pos.X - offset;
	}
	else if(direction == UP)
	{
		pos.Y = pos.Y - offset;
	}
	else if(direction == DOWN)
	{
		pos.Y = pos.Y + offset;
	}
}


void Point::Clear ()
{
	this->sym = ' ';
	Draw();
}


bool Point::IsHit(Point p)
{
	return p.pos.X == this->pos.X && p.pos.Y == this->pos.Y;
}