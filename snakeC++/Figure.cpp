#include "stdafx.h"
#include "Figure.h"



Figure::Figure(void)
{
}


Figure::~Figure(void)
{
}


void Figure::Draw()
{
	std::list<Point>::iterator it;
	for(it = pList.begin(); it != pList.end(); it++)
	{
		it->Draw();
	};
}


bool Figure::IsHit( Figure line )
{
	std::list<Point>::iterator it;
	for(it = pList.begin(); it != pList.end(); it++)
	{
		if ( line.IsHit( *it ) )
			return true;
	}
	return false;
}

bool Figure::IsHit( Point point )
{
	std::list<Point>::iterator it;
	for(it = pList.begin(); it != pList.end(); it++)
	{
		if ( it->IsHit( point ) )
			return true;
	}
	return false;
}