#pragma once
#include "Direction.h"

class Point
{
public:
	COORD pos;
	char sym;
	static HANDLE hOuput;
	Point(int _x, int _y, char _sym);
	~Point(void);
	void Draw();
	void Move(int offset, Direction direction);
	void Clear();
	bool Point::IsHit(Point);
};

