#include "stdafx.h"
#include "Point.h"
#include "HorizontalLine.h"
#include "Figure.h"


HorizontalLine::HorizontalLine(int xLeft, int xRight, int y, char sym)
{
	for (int x = xLeft; x <= xRight; x++)
	{
		Point p = Point(x,y,sym);
		pList.push_back(p);
	}
}


HorizontalLine::~HorizontalLine(void)
{
}
