#pragma once
#include "Figure.h"
class Walls
{
	std::list<Figure> wallList;
public:
	Walls(void);
	~Walls(void);
	Walls(COORD screenSize);
	void Draw();
	bool IsHit( Figure figure);
};

