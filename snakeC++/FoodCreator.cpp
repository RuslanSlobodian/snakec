#include "stdafx.h"
#include "FoodCreator.h"
#include <ctime>

FoodCreator::~FoodCreator(void)
{
}


FoodCreator::FoodCreator(int mapWidth, int mapHeight, char sym)
{
	this->mapWidht = mapWidth;
	this->mapHeight = mapHeight;
	this->sym = sym;
}

Point FoodCreator::CreateFood()
{
	srand( time(0) );

	int x = rand() % (this->mapWidht - 2) + 1;
	int y = rand() % (this->mapHeight - 2) + 1;
	Point *newfood = new Point ( x, y, sym );
	return *newfood;
}