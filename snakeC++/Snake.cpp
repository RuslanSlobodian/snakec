#include "stdafx.h"
#include "Figure.h"
#include "Direction.h"
#include "Point.h"
#include "Snake.h"


Snake::Snake(Point tail, int length, Direction _direction )
{
	direction = _direction;
	for (int i = 0; i < length; i++)
	{
		Point p(tail);
		p.Move(i, direction);
		pList.push_back(p);
	}


}


Snake::~Snake(void)
{
}


void Snake::Move()
{
	Point tail = pList.front();
	pList.pop_front();
	Point head = this->GetNextPoint();
	pList.push_back(head);
	tail.Clear();
	head.Draw();
}

inline Point Snake::GetNextPoint()
{
	Point nextPoint(pList.back());
	nextPoint.Move(1,direction);
	return nextPoint;
}

void Snake::HandleKey(int key)
{
	switch(key)
	{	
	case 72:	//UP
		if (direction != DOWN)
			direction = UP;
		break;
	case 80:	//DOWN
		if (direction != UP)
			direction = DOWN;
		break;
	case 75:	//LEFT
		if (direction != RIGHT)
			direction = LEFT;
		break;
	case 77:	//RIGHT
		if (direction != LEFT)
			direction =RIGHT;
		break;
	}

}


bool Snake::IsHitTail()
{
	Point head = pList.back();
	std::list<Point>::iterator it = pList.begin();
	//std::list<Point>::iterator end;
	int size = pList.size();

	for(int i = 0; i < size-1; i++, it++)
	{
		if ( head.IsHit( *it ) )
			return true;
	}
	return false;
}

bool Snake::Eat( Point food )
{
	Point head = GetNextPoint();
	if ( head.IsHit( food ) )
	{
		food = head;
		head.Draw();
		pList.push_back( food );
		return true;
	}
	else
		return false;
}