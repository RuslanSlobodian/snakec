#include "stdafx.h"
#include "Point.h"
#include "VerticalLine.h"
#include "Figure.h"

VerticalLine::VerticalLine(int x, int yTop, int yBottom, char sym)
{
	for (int y = yTop; y <= yBottom; y++)
	{
		Point p = Point(x,y,sym);
		pList.push_back(p);
	}
}


VerticalLine::~VerticalLine(void)
{
}
