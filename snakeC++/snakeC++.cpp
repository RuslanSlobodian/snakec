// snakeC++.cpp: ���������� ����� ����� ��� ����������� ����������.
//

//���������� ��� "stdafx.h" ���: https://habrahabr.ru/company/pvs-studio/blog/227521/
#include "stdafx.h"
#include "Point.h"
#include "HorizontalLine.h"
#include "VerticalLine.h"
#include "Snake.h"
#include "Direction.h"
#include "FoodCreator.h"
#include "Walls.h"


void myfunction (int i) 
{
	std::cout << ' ' << i;
}

int main()
{
	COORD screenSize;
	screenSize.X = 80;
	screenSize.Y = 25;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), screenSize);
	int timeToSleep = 200; 

	// ���� ��� �������� ����
	Walls walls( screenSize );
	walls.Draw();

	// ���� �����
	Point p (4, 5, '*');
	Snake snake(p,4,RIGHT);
	snake.Draw();

	FoodCreator foodCreator( 80, 25, '$' );
	Point food = foodCreator.CreateFood();
	food.Draw();

	int ch = 0;
	while(ch != 27) 
	{
		if ( walls.IsHit(snake) || snake.IsHitTail() )
		{
			COORD pos;
			pos.X = screenSize.X / 2 - 5;
			pos.Y = screenSize.Y / 2;
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
			SetConsoleTextAttribute( GetStdHandle(STD_OUTPUT_HANDLE), (WORD)((4 << 4) | 15) );
			std::cout<<"GAME OVER!";
			break;
		}
		if(snake.Eat( food ) )
		{
			food = foodCreator.CreateFood();
			food.Draw();
			timeToSleep-=10;
		}
		else
		{
			snake.Move();
		}
		Sleep( timeToSleep );
		fflush(stdin);
		for (int i = 0; i < 2; i++)
		{
			if (_kbhit())
			{
				ch = _getch();
				snake.HandleKey( ch );
			}
		}

	}

	system("pause>null");
	return 0;
}

